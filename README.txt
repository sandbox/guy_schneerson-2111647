
Commerce Pay on collection for drupal commerce.

An offsite payment method that allows the user to specify where he will pickup
his order and pay for it.

Installation
1. Download and Install the module
2. Configure the Rule "Pay on collection" via Store > Configuration >
  Payment settings.
3. Edit the action "Enable payment method: Pay on collection"
4. Add a list of collection points (see below)

collection points
=================
the format is name|details where the name is the internal name and details
is the text that will show to the user.
The following is an example:
blu-bag brighton|Brighton branch, New England House, Brighton
blue-bag chilcompton|Chilcompton branch, Manor Farm, Chilcompton

Note: The name is also used when creating the payment transection message.
